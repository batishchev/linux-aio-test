TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -Wextra -pedantic -fnon-call-exceptions -std=gnu++11

SOURCES += main.cpp

unix|win32: LIBS += -lboost_regex
