/*
 *  eventfd-aio-test by Davide Libenzi (test app for eventfd hooked into KAIO)
 *  Copyright (C) 2007  Davide Libenzi
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Davide Libenzi <davidel@xmailserver.org>
 *
 */

//#define _GNU_SOURCE
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <sys/mman.h>
//#include <sys/poll.h>
#include <sys/epoll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>

#include <vector>
#include <string>
#include <iostream>

#include <boost/regex.hpp>

#define BLKRRPART  _IO(0x12,95)     // re-read partition table
#define BLKSSZGET  _IO(0x12,104)	// get block device sector size
#define BLKGETSIZE _IO(0x12,96)	    // return device size /sect_size (long *arg)

#define BUILD_BUG_IF(c) ((void) sizeof(char[1 - 2 * !!(c)]))

#define TESTFILE_SIZE (4096 * 5200)
#define NUM_EVENTS 64
//#define IORTX_SIZE (1024 * 4)
#define IORTX_SIZE NUM_EVENTS


typedef unsigned long aio_context_t;

enum {
    IOCB_CMD_PREAD = 0,
    IOCB_CMD_PWRITE = 1,
    IOCB_CMD_FSYNC = 2,
    IOCB_CMD_FDSYNC = 3,
    /* These two are experimental.
     * IOCB_CMD_PREADX = 4,
     * IOCB_CMD_POLL = 5,
     */
    IOCB_CMD_NOOP = 6,
    IOCB_CMD_PREADV = 7,
    IOCB_CMD_PWRITEV = 8,
};

#if defined(__LITTLE_ENDIAN)
#define PADDED(x,y)	x, y
#elif defined(__BIG_ENDIAN)
#define PADDED(x,y)	y, x
#else
#error edit for your odd byteorder.
#endif

#define IOCB_FLAG_RESFD		(1 << 0)

/*
 * we always use a 64bit off_t when communicating
 * with userland.  its up to libraries to do the
 * proper padding and aio_error abstraction
 */
struct iocb
{
    /* these are internal to the kernel/libc. */
    u_int64_t	aio_data;	/* data to be returned in event's data */
    u_int32_t	PADDED(aio_key, aio_reserved1);
    /* the kernel sets aio_key to the req # */

    /* common fields */
    u_int16_t	aio_lio_opcode;	/* see IOCB_CMD_ above */
    int16_t     aio_reqprio;
    u_int32_t	aio_fildes;

    u_int64_t	aio_buf;        // buff ptr
    u_int64_t	aio_nbytes;     // length of the buff
    int64_t     aio_offset;     // write offset

    /* extra parameters */
    u_int64_t	aio_reserved2;	/* TODO: use this for a (struct sigevent *) */

    u_int32_t	aio_flags;
    /*
     * If different from 0, this is an eventfd to deliver AIO results to
     */
    u_int32_t	aio_resfd;
}; /* 64 bytes */

struct io_event
{
    u_int64_t         data;           /* the data field from the iocb */
    u_int64_t         obj;            /* what iocb this event came from */
    int64_t           res;            /* result code for this event */
    int64_t           res2;           /* secondary result */
};

int64_t
disk_lba_qty(int fd)
{
    unsigned long qty=0;
    if(-1==ioctl(fd, BLKGETSIZE, &qty) || qty==0)
    {
        return 0;
    }

    return qty;
}

int
disk_lba_size(int fd)
{
    int lba_size=0;
    if(-1==ioctl(fd, BLKSSZGET, &lba_size)||lba_size==0)
    {
        return 0;
    }

    return lba_size;
}

static void asyio_prep_preadv(struct iocb *iocb, int fd, struct iovec *iov, int nr_segs, int64_t offset, int afd)
{
    memset(iocb, 0, sizeof(*iocb));
    iocb->aio_fildes = fd;
    iocb->aio_lio_opcode = IOCB_CMD_PREADV;
    iocb->aio_reqprio = 0;
    iocb->aio_buf = (u_int64_t) iov;
    iocb->aio_nbytes = nr_segs;
    iocb->aio_offset = offset;
    iocb->aio_flags = IOCB_FLAG_RESFD;
    iocb->aio_resfd = afd;
}

static void asyio_prep_pwritev(struct iocb *iocb, int fd, struct iovec *iov, int nr_segs, int64_t offset, int afd)
{
    memset(iocb, 0, sizeof(*iocb));
    iocb->aio_fildes = fd;
    iocb->aio_lio_opcode = IOCB_CMD_PWRITEV;
    iocb->aio_reqprio = 0;
    iocb->aio_buf = (u_int64_t) iov;
    iocb->aio_nbytes = nr_segs;
    iocb->aio_offset = offset;
    iocb->aio_flags = IOCB_FLAG_RESFD;
    iocb->aio_resfd = afd;
}

static void asyio_prep_pread(struct iocb *iocb, int fd, void *buf, int nr_segs, int64_t offset, int afd)
{
    memset(iocb, 0, sizeof(*iocb));
    iocb->aio_fildes = fd;
    iocb->aio_lio_opcode = IOCB_CMD_PREAD;
    iocb->aio_reqprio = 0;
    iocb->aio_buf = (u_int64_t) buf;
    iocb->aio_nbytes = nr_segs;
    iocb->aio_offset = offset;
    iocb->aio_flags = IOCB_FLAG_RESFD;
    iocb->aio_resfd = afd;
}

static void asyio_prep_pwrite(struct iocb *iocb, int fd, void const *buf, int nr_segs, int64_t offset, int afd)
{
    memset(iocb, 0, sizeof(*iocb));
    iocb->aio_fildes = fd;
    iocb->aio_lio_opcode = IOCB_CMD_PWRITE;
    iocb->aio_reqprio = 0;
    iocb->aio_buf = (u_int64_t) buf;
    iocb->aio_nbytes = nr_segs;
    iocb->aio_offset = offset;
    iocb->aio_flags = IOCB_FLAG_RESFD;
    iocb->aio_resfd = afd;
}

static long io_setup(unsigned nr_reqs, aio_context_t *ctx)
{
    return syscall(__NR_io_setup, nr_reqs, ctx);
}

static long io_destroy(aio_context_t ctx)
{
    return syscall(__NR_io_destroy, ctx);
}

static long io_submit(aio_context_t ctx, long n, struct iocb **paiocb)
{
    return syscall(__NR_io_submit, ctx, n, paiocb);
}

static long io_cancel(aio_context_t ctx, struct iocb *aiocb, struct io_event *res)
{
    return syscall(__NR_io_cancel, ctx, aiocb, res);
}

static long io_getevents(aio_context_t ctx, long min_nr, long nr, struct io_event *events, struct timespec *tmo)
{
    return syscall(__NR_io_getevents, ctx, min_nr, nr, events, tmo);
}

static int eventfd(int count, int flags)
{
    return syscall(__NR_eventfd2, count, flags);
}

static long pwaitasync(int ee, int timeo)
{
    /*
    struct pollfd pfd;

    pfd.fd = afd;
    pfd.events = POLLIN;
    pfd.revents = 0;

    if (poll(&pfd, 1, timeo) < 0)
    {
        perror("poll");
        return -1;
    }
    if ((pfd.revents & POLLIN) == 0)
    {
        fprintf(stderr, "no results completed\n");
        return 0;
    }
*/

    epoll_event eevt[10];


    return epoll_wait(ee, eevt, 10, timeo);
}

//////////////////////////////////////////////////////////

static long test_read(int ee, aio_context_t ctx, int fd, int fd1, long range, int afd)
{
    long i, n, r, r1, j;
    u_int64_t eval;
    struct iocb **piocb, **piocb1;
    struct iocb *iocb, *iocb1;
    struct timespec tmo;
    static struct io_event events[NUM_EVENTS];
    static char buf[IORTX_SIZE];

    n = range / IORTX_SIZE;
    iocb = static_cast<struct iocb*>(malloc(n * sizeof(struct iocb)));
    piocb = static_cast<struct iocb **>(malloc(n * sizeof(struct iocb *)));

    if (!iocb || !piocb)
    {
        perror("iocb alloc");
        return -1;
    }

    iocb1 = static_cast<struct iocb*>(malloc(n * sizeof(struct iocb)));
    piocb1 = static_cast<struct iocb **>(malloc(n * sizeof(struct iocb *)));

    if (!iocb1 || !piocb1)
    {
        perror("iocb alloc 1");
        return -1;
    }

    for (i = 0; i < n; i++)
    {
        piocb[i] = &iocb[i];
        asyio_prep_pread(&iocb[i], fd, buf, sizeof(buf), (n - i - 1) * IORTX_SIZE, afd);
        iocb[i].aio_data = (u_int64_t) i + 1;

        piocb1[i] = &iocb1[i];
        asyio_prep_pread(&iocb1[i], fd1, buf, sizeof(buf), (n - i - 1) * IORTX_SIZE, afd);
        iocb1[i].aio_data = (u_int64_t) i + 1;
    }

    fprintf(stdout, "submitting read requests (%ld) ...\n", n*2);
    if ((r = io_submit(ctx, n, piocb)) <= 0)
    {
        perror("io_submit");
        return -1;
    }

    if ((r1 = io_submit(ctx, n, piocb1)) <= 0)
    {
        perror("io_submit 1");
        return -1;
    }

    fprintf(stdout, "submitted %ld requests\n", r+r1);
    for (i = 0; i < n*2;)
    {
        fprintf(stdout, "waiting ... "), fflush(stdout);
        if( -1 == pwaitasync(ee, -1))
        {
            perror("epoll_wait");
            return -1;
        }
        eval = 0;
        if (read(afd, &eval, sizeof(eval)) != sizeof(eval))
        {
            perror("read");
        }

        fprintf(stdout, "done! %llu\n", (unsigned long long) eval);

        while (eval > 0)
        {
            tmo.tv_sec = 0;
            tmo.tv_nsec = 0;
            r = io_getevents(ctx, 1, eval > NUM_EVENTS ? NUM_EVENTS: (long) eval, events, &tmo);
            if (r > 0)
            {
                for (j = 0; j < r; j++)
                {

                }
                i += r;
                eval -= r;
                fprintf(stdout, "test_read got %ld/%ld results so far\n", i, n*2);
            }
        }
    }

    free(iocb);
    free(piocb);

    free(iocb1);
    free(piocb1);

    return n*2;
}

static long test_write(int ee, aio_context_t ctx, int fd, int fd1, long range, int afd)
{
    long i, n, r, r1, j;
    u_int64_t eval; // must be 8 bytes
    struct iocb **piocb, **piocb1;
    struct iocb *iocb, *iocb1;
    struct timespec tmo;
    static struct io_event events[NUM_EVENTS];
    static char buf[IORTX_SIZE];

    for (i = 0; i < IORTX_SIZE; i++)
    {
        buf[i] = i & 0xff;
    }

    n = range / IORTX_SIZE;
    iocb = static_cast<struct iocb *>(malloc(n * sizeof(struct iocb)));
    piocb = static_cast<struct iocb **>(malloc(n * sizeof(struct iocb *)));

    if (!iocb || !piocb)
    {
        perror("iocb alloc 1");
        return -1;
    }

    iocb1 = static_cast<struct iocb *>(malloc(n * sizeof(struct iocb)));
    piocb1 = static_cast<struct iocb **>(malloc(n * sizeof(struct iocb *)));

    if (!iocb1 || !piocb1)
    {
        perror("iocb alloc 2");
        return -1;
    }

    for (i = 0; i < n; i++)
    {
        piocb[i] = &iocb[i];
        asyio_prep_pwrite(&iocb[i], fd, buf, sizeof(buf),  (n - i - 1) * IORTX_SIZE, afd);
        iocb[i].aio_data = (u_int64_t) i + 1;

        piocb1[i] = &iocb1[i];
        asyio_prep_pwrite(&iocb1[i], fd1, buf, sizeof(buf),  (n - i - 1) * IORTX_SIZE, afd);
        iocb1[i].aio_data = (u_int64_t) i + 1;

    }

    fprintf(stdout, "submitting write requests (%ld) ...\n", n*2);
    if ((r = io_submit(ctx, n, piocb)) <= 0)
    {
        perror("io_submit");
        return -1;
    }

    if ((r1 = io_submit(ctx, n, piocb1)) <= 0)
    {
        perror("io_submit 1");
        return -1;
    }

    fprintf(stdout, "submitted %ld requests\n", r+r1);

//    free(iocb);
//    free(piocb);

//    free(iocb1);
//    free(piocb1);

    for (i = 0; i < n*2;)
    {
        fprintf(stdout, "waiting ... "), fflush(stdout);

        if(-1==pwaitasync(ee, -1))
        {
            perror("epoll_wait");
            return -1;
        }

        eval = 0;
        if(read(afd, &eval, sizeof(eval)) != sizeof(eval))
        {
            perror("read");
        }
        fprintf(stdout, "done! %llu\n", (unsigned long long) eval);
        while (eval > 0)
        {
            tmo.tv_sec = 0;
            tmo.tv_nsec = 0;
            r = io_getevents(ctx, 1, eval > NUM_EVENTS ? NUM_EVENTS: (long) eval, events, &tmo);
            if (r > 0)
            {
                for (j = 0; j < r; j++)
                {

                }
                i += r;
                eval -= r;
                fprintf(stdout, "test_write got %ld/%ld results so far\n", i, n*2);
            }
        }
    }

    free(iocb);
    free(piocb);

    free(iocb1);
    free(piocb1);

    return n;
}

//////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////

std::string
_load()
{
    char buff[1024];

    std::string str;
    int fd;

    if((fd = open("/proc/meminfo", O_RDONLY ))<0)
    {
        return str;
    }

    str.reserve(4096);

    int res_len;

    while((res_len = read(fd, buff, 1023 ))>0)
    {
        buff[res_len] = '\0';
        str.append(buff);
    }

    if(res_len<0)
    {
        str = std::string(); // we read all or nothing
    }

    close(fd);

    return str;
}

u_int64_t
_get_value(const std::string &meminfo, const std::string &reg)
{
    int64_t val=0, mult;
    boost::regex rx(reg);
    boost::match_results<std::string::const_iterator> res;

    boost::regex_search(meminfo, res, rx);

    int subs = res.size();

    if(subs<2) return 0;

    std::string str_val, str_mult;
    str_val = res[1];
    if(subs>2)
    {
        str_mult = res[2];
    }

    std::istringstream ii(str_val);

    ii >> val;
    if(str_mult == "k" || str_mult=="K") mult = 1024;
    else if(str_mult=="m" || str_mult=="M") mult=1024L*1024L;
    else if(str_mult=="g" || str_mult=="G") mult=1024L*1024L*1024L;
    else mult=1;

    return val*mult;
}

u_int64_t
_get_value(const std::string &reg)
{
    return _get_value(_load(), reg);
}

u_int64_t
huge_pages_size()
{
    static u_int64_t _huge_page_size;

    if(_huge_page_size) return _huge_page_size;

    _huge_page_size = _get_value("Hugepagesize:\\s+(\\d+)\\s?(\\w)B");
    if(!_huge_page_size)  _huge_page_size = 4096;
    return _huge_page_size;
}

u_int64_t
align(u_int64_t size)
{
    u_int64_t page_size = huge_pages_size();

    if(size < page_size)
    {
        size = page_size;
    }
    else if(size > page_size)
    {
        size = (size & ~(page_size-1)) + (size & (page_size-1)?page_size:0);
    }

    return size;
}

u_int64_t
align_less(u_int64_t size)
{
    u_int64_t page_size = huge_pages_size();

    size = (size & ~(page_size-1));

    return size;
}


void *
_alloc_huge_block(u_int64_t size)
{
    if(!size) size = huge_pages_size(); // allocate one page

    return mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_HUGETLB | MAP_PRIVATE /* MAP_SHARED*/, -1, 0);
}

void *
alloc_huge_block(u_int64_t size=0)
{

    void *ptr = _alloc_huge_block(align(size));
    if(ptr == MAP_FAILED)
    {
        ptr = nullptr;
    }
    return ptr;
}

int
free_huge_block(void *mem_block, size_t len=0)
{
    return munmap(mem_block, len==0?huge_pages_size():len);
}


#define QtyBuffers (((n*2)-nr)>IORTX_SIZE?IORTX_SIZE:((n*2)-nr))

static long test_read_huge(int ee, aio_context_t ctx, int fd, int fd1, uint64_t range, int afd)
{
    long i, n, nc, nr, r, r1, j;
    u_int64_t eval;
    struct iocb **piocb, **piocb1;
    struct iocb *iocb, *iocb1;
    struct timespec tmo;
    struct io_event events[NUM_EVENTS];
    char *buf[IORTX_SIZE];
    char *buf1[IORTX_SIZE];
    char *huge_pages[IORTX_SIZE];
    u_int64_t page_size = huge_pages_size();
    u_int64_t buff_size =  1024*1024;

    if((buff_size * 2) != page_size)
    {
        perror("page_size");
        return -1;
    }

    n = align_less(range) / buff_size;  // total blocks to read
    nc = 0;                             // current submitted block
    nr = 0;                             //current received block

    iocb = static_cast<struct iocb*>(malloc(QtyBuffers * sizeof(struct iocb)));
    piocb = static_cast<struct iocb **>(malloc(QtyBuffers * sizeof(struct iocb *)));

    if (!iocb || !piocb)
    {
        perror("iocb alloc");
        return -1;
    }

    iocb1 = static_cast<struct iocb*>(malloc(QtyBuffers * sizeof(struct iocb)));
    piocb1 = static_cast<struct iocb **>(malloc(QtyBuffers * sizeof(struct iocb *)));

    if (!iocb1 || !piocb1)
    {
        perror("iocb alloc 1");
        return -1;
    }

    for(i=0;i<IORTX_SIZE;++i)
    {
        huge_pages[i] = static_cast<char *>(alloc_huge_block());
        if(huge_pages[i]==nullptr)
        {
            perror("cannot allocate block");
            return -1;
        }
    }

    // loop untill we wrote all range
    while (nr<(n*2))
    {

        // initialize io buffs for request
        for (i = 0; i < QtyBuffers; i++, nc++)
        {
            // assign buffer to io operation
            buf[i] = huge_pages[i];
            buf1[i] = huge_pages[i]+buff_size;

            // prepare io operation request
            piocb[i] = &iocb[i];
            u_int64_t offset = nc * buff_size;
            asyio_prep_pread(&iocb[i], fd, buf[i], buff_size, offset, afd);
            iocb[i].aio_data = nc;

            piocb1[i] = &iocb1[i];
            asyio_prep_pread(&iocb1[i], fd1, buf1[i], buff_size, offset, afd);
            iocb1[i].aio_data = nc;
        }


        fprintf(stdout, "submitting read requests (%ld) ...\n", QtyBuffers*2);

        if ((r = io_submit(ctx, QtyBuffers, piocb)) <= 0)
        {
            perror("io_submit");
            return -1;
        }

        if ((r1 = io_submit(ctx, QtyBuffers, piocb1)) <= 0)
        {
            perror("io_submit 1");
            return -1;
        }

        int rs = r+r1;      // qty submitted requests
        int rr=0;           // qty received requests

        fprintf(stdout, "submitted %d requests\n", rs);
        fprintf(stdout, "waiting ... "), fflush(stdout);

        if( -1 == pwaitasync(ee, -1))
        {
            perror("epoll_wait");
            return -1;
        }
        eval = 0;
        if (read(afd, &eval, sizeof(eval)) != sizeof(eval))
        {
            perror("read eventfd");
        }

        fprintf(stdout, "eventfd value: %llu\n", (unsigned long long) eval);

        while (rr<rs)
        {
            tmo.tv_sec = 3;
            tmo.tv_nsec = 0;
            int read_events = rs-rr;

            r = io_getevents(ctx, 1, read_events > NUM_EVENTS ? NUM_EVENTS: (long) read_events, events, &tmo);
            if (r > 0)
            {
                rr += r;
                nr += r;
                for (j = 0; j < r; j++)
                {
                    if(events[j].res != buff_size )
                    {
                        fprintf(stdout, "iocb returned data: %llX result: %ld\n", events[j].data, events[j].res);
                    }
                    else
                    {
                        struct iocb *iocb = reinterpret_cast<struct iocb *>(events[j].obj);
                        if(iocb->aio_data!=events[j].data)
                        {
                            perror("iocb addessing");
                        }
                        else
                        {
                            int errs=0;
                            uint64_t val = events[j].data;
                            uint64_t * buf = reinterpret_cast<uint64_t *>(iocb->aio_buf);
                            size_t qty_loops = iocb->aio_nbytes / sizeof(uint64_t);
                            for(size_t k=0;k<qty_loops;++k)
                            {
                                if(*(buf++)!=val)  errs++;
                            }
                            if(errs)
                            {
                                perror("received invalid data");
                            }
                        }
                    }
                }
            }
            else if(r==0)
            {
                fprintf(stdout, "io_getevents timeout\n");
            }
            else
            {
                perror("io_getevents\n");
                return -1;
            }
        }
        fprintf(stdout, "test_read got %ld/%ld results so far\n", nr, n*2);
    }

    // alloc buffers
    for(i=0;i<IORTX_SIZE; i++)
    {
        free_huge_block(huge_pages[i]);
    }

    free(iocb);
    free(piocb);

    free(iocb1);
    free(piocb1);

    return nr;
}

static long test_write_huge(int ee, aio_context_t ctx, int fd, int fd1, uint64_t range, int afd)
{
    long i, n, nc, nr, r, r1, j;
    u_int64_t eval;
    struct iocb **piocb, **piocb1;
    struct iocb *iocb, *iocb1;
    struct timespec tmo;
    struct io_event events[NUM_EVENTS];
    char *buf[IORTX_SIZE];
    char *buf1[IORTX_SIZE];
    char *huge_pages[IORTX_SIZE];
    u_int64_t page_size = huge_pages_size();
    u_int64_t buff_size =  1024*1024;

    if((buff_size * 2) != page_size)
    {
        perror("page_size");
        return -1;
    }

    n = align_less(range) / buff_size;  // total blocks to read  - per disk
    nc = 0;                             // current submitted blocks  -  per disk
    nr = 0;                             // current qty blocks received  -  total

    iocb = static_cast<struct iocb*>(malloc(QtyBuffers * sizeof(struct iocb)));
    piocb = static_cast<struct iocb **>(malloc(QtyBuffers * sizeof(struct iocb *)));

    if (!iocb || !piocb)
    {
        perror("iocb alloc");
        return -1;
    }

    iocb1 = static_cast<struct iocb*>(malloc(QtyBuffers * sizeof(struct iocb)));
    piocb1 = static_cast<struct iocb **>(malloc(QtyBuffers * sizeof(struct iocb *)));

    if (!iocb1 || !piocb1)
    {
        perror("iocb alloc 1");
        return -1;
    }

    for(i=0;i<IORTX_SIZE;++i)
    {
        huge_pages[i] = static_cast<char *>(alloc_huge_block());
        if(huge_pages[i]==nullptr)
        {
            perror("cannot allocate block");
            return -1;
        }
    }

    // loop untill we wrote all range
    while (nr<(n*2))
    {

        // initialize io buffs for request
        for (i = 0; i < QtyBuffers; i++, nc++)
        {
            // prepare buffer
            uint64_t *buf64 = reinterpret_cast<uint64_t *>(huge_pages[i]);
            for(uint32_t j=0; j<page_size/sizeof(uint64_t);j++)
            {
                *(buf64++) = nc;
            }

            // assign buffer to io operation
            buf[i] = huge_pages[i];
            buf1[i] = huge_pages[i]+buff_size;

            // prepare io operation request
            piocb[i] = &iocb[i];
            u_int64_t offset = nc * buff_size;
            asyio_prep_pwrite(&iocb[i], fd, buf[i], buff_size, offset, afd);
            iocb[i].aio_data = nc;

            piocb1[i] = &iocb1[i];
            asyio_prep_pwrite(&iocb1[i], fd1, buf1[i], buff_size, offset, afd);
            iocb1[i].aio_data = nc;
        }


        fprintf(stdout, "submitting write requests (%ld) ...\n", QtyBuffers*2);

        if ((r = io_submit(ctx, QtyBuffers, piocb)) <= 0)
        {
            perror("io_submit");
            return -1;
        }

        if ((r1 = io_submit(ctx, QtyBuffers, piocb1)) <= 0)
        {
            perror("io_submit 1");
            return -1;
        }

        int rs = r+r1;      // qty submitted requests
        int rr=0;           // qty received requests

        fprintf(stdout, "submitted %d requests\n", rs);
        fprintf(stdout, "waiting ... "), fflush(stdout);

        if( -1 == pwaitasync(ee, -1))
        {
            perror("epoll_wait");
            return -1;
        }
        eval = 0;
        if (read(afd, &eval, sizeof(eval)) != sizeof(eval))
        {
            perror("read");
        }


        fprintf(stdout, "eventfd value: %llu\n", (unsigned long long) eval);

        while (rr<rs) // get response to all requests we sent
        {
            tmo.tv_sec = 3;
            tmo.tv_nsec = 0;
            int read_events = rs-rr;

            r = io_getevents(ctx, 1, read_events > NUM_EVENTS ? NUM_EVENTS: (long) read_events, events, &tmo);
            if(r > 0)
            {
                rr += r;
                nr += r;
                for (j = 0; j < r; j++)
                {
                    if(events[j].res != buff_size )
                    {
                        fprintf(stdout, "iocb returned data: %llX result: %ld\n", events[j].data, events[j].res);
                    }
                }
            }
            else if(r==0)
            {
                fprintf(stdout, "io_getevents timeout\n");
            }
            else
            {
                perror("io_getevents");
                return -1;
            }
        }
        fprintf(stdout, "test_write got %ld/%ld results so far\n", nr, n*2);
    }

    // alloc buffers
    for(i=0;i<IORTX_SIZE; i++)
    {
        free_huge_block(huge_pages[i]);
    }

    free(iocb);
    free(piocb);

    free(iocb1);
    free(piocb1);

    return nr;
}

//////////////////////////////////////////////////////////


int main(int ac, char **av)
{
    int afd, fd_1, fd_2, epfd;
    aio_context_t ctx = 0;
    char const *disk_1 = "/dev/sda";
    char const *disk_2 = "/dev/sdb";
    int opt;
    bool read_test=false, write_test=false;

    while((opt=getopt(ac, av, "rw"))!=-1)
    {
        switch(opt)
        {
            case 'r': read_test = true; break;
            case 'w': write_test = true; break;
        }
    }

    if(!read_test && !write_test)
    {
        fprintf(stdout, "usage aio -[r]|[w]\n");
        return 0;
    }

    BUILD_BUG_IF(sizeof(struct iocb) != 64);

    fprintf(stdout, "creating an eventfd ...\n");
    if ((afd = eventfd(0, O_NONBLOCK)) == -1)
    {
            perror("eventfd");
            return 2;
    }
    fprintf(stdout, "done! eventfd = %d\n", afd);


    if(-1 == (epfd = epoll_create1(0)))
    {
            perror("epoll_create");
            return 1;
    }

    if (io_setup((TESTFILE_SIZE / IORTX_SIZE + 256)*2, &ctx))
    {
        perror("io_setup");
        return 3;
    }

    if ((fd_1 = open(disk_1, O_RDWR | O_DIRECT, 0)) == -1)
    {
        perror(disk_1);
        return 4;
    }

    if ((fd_2 = open(disk_2, O_RDWR | O_DIRECT, 0)) == -1)
    {
        perror(disk_1);
        return 4;
    }

    fcntl(fd_1, F_SETFL, fcntl(fd_1, F_GETFL, 0) | O_NONBLOCK);
    fcntl(fd_2, F_SETFL, fcntl(fd_2, F_GETFL, 0) | O_NONBLOCK);

    fcntl(afd, F_SETFL, fcntl(afd, F_GETFL, 0) | O_NONBLOCK);

    epoll_event eevt;
    eevt.events = EPOLLIN | EPOLLET | EPOLLERR;
    eevt.data.fd = afd;

    if( -1 == epoll_ctl(epfd, EPOLL_CTL_ADD, afd, &eevt))
    {
        perror("epoll_ctl");
        return 5;
    }

    uint64_t volume_size = disk_lba_qty(fd_1) * disk_lba_size(fd_1);

    if(write_test)
    {
        test_write_huge(epfd, ctx, fd_1, fd_2, volume_size, afd);
        fprintf(stdout, "volume fill in completed\n start read and check\n");
    }
    if(read_test)
    {
        test_read_huge(epfd, ctx, fd_1, fd_2, volume_size, afd);
        fprintf(stdout, "start read and check completed\n");
    }

    io_destroy(ctx);
    close(fd_1);
    close(fd_2);
    close(afd);
    close(epfd);

    fprintf(stdout, "Done. Press any key...\n");
    pause();

    //remove(disk_1);
    //remove(disk_2);

    return 0;
}
